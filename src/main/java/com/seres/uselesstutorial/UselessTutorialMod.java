package com.seres.uselesstutorial;

import net.fabricmc.api.ModInitializer;

public class UselessTutorialMod implements ModInitializer
{
    public static String MODID = "uselesstutorial";

    @Override
    public void onInitialize()
    {
        System.out.println("Loading " + MODID);
    }
}
