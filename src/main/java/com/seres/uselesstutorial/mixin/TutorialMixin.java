package com.seres.uselesstutorial.mixin;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.RunArgs;
import net.minecraft.client.tutorial.TutorialManager;
import net.minecraft.client.tutorial.TutorialStep;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MinecraftClient.class)
public abstract class TutorialMixin
{
    @Shadow
    private static MinecraftClient instance;

    @Shadow @Final private TutorialManager tutorialManager;

    @Inject(at = @At("RETURN"), method = "<init>")
    public void disableTutorial(RunArgs args, CallbackInfo ci)
    {
        if (instance.options.tutorialStep != TutorialStep.NONE) {
            System.out.println("Setting tutorial step to none");
            tutorialManager.setStep(TutorialStep.NONE);
        }
    }
}
